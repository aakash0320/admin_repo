from django.contrib import admin
from django.db import models
from test_temp.models import language,writer
from test_temp.models import student

admin.site.register(language)
admin.site.register(student)
admin.site.register(writer)